# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with links' do |factory|
  before do
    @object = create(factory)
  end

  context 'signed in and admin of object' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @user.add_role :admin, @object
    end

    it 'can get the links' do
      links = create_list(:external_link, 3)
      links.each { |link| @object.external_links << link }

      get :index_link, params: { id: @object.id }

      expect(response).to have_http_status :ok
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq 3
      expect(json_response.pluck('id')).to include(*links.map(&:id))
    end

    it 'can add a link' do
      icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')
      expect do
        post :create_link, params: { id: @object.id, url: 'http://something.com', name: 'custom', icon: icon }
      end.to change(ExternalLink, :count).by(1)
      expect(response).to have_http_status :created
      json_response = JSON.parse(response.body)
      expect(json_response['url']).to match 'http://something.com'
      expect(json_response['icon_url']).not_to be_falsey
    end

    it 'can update a link' do
      icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')
      link = create(:external_link)
      @object.external_links << link
      patch :update_link, params: { id: @object.id, link_id: link.id, url: 'http://something.com', name: 'custom', icon: icon }
      expect(response).to have_http_status :ok
      json_response = JSON.parse(response.body)
      expect(json_response['url']).to match 'http://something.com'
      expect(json_response['icon_url']).not_to be_falsey
    end

    it 'can destroy a link' do
      link = create(:external_link)
      @object.external_links << link
      expect do
        delete :destroy_link, params: { id: @object.id, link_id: link.id }
      end.to change(ExternalLink, :count).by(-1)
      expect(response).to have_http_status :ok
    end
  end

  context 'signed in and not admin of object' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
    end

    it 'can get the links' do
      links = create_list(:external_link, 3)
      links.each { |link| @object.external_links << link }

      get :index_link, params: { id: @object.id }

      expect(response).to have_http_status :ok
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq 3
      expect(json_response.pluck('id')).to include(*links.map(&:id))
    end

    it 'cannot add a link' do
      icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')

      expect do
        post :create_link, params: { id: @object.id, url: 'http://something.com', name: 'custom', icon: icon }
      end.to change(ExternalLink, :count).by(0)
      expect(response).to have_http_status :forbidden
    end

    it 'cannot update a link' do
      icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')
      link = create(:external_link)
      @object.external_links << link
      patch :update_link, params: { id: @object.id, link_id: link.id, url: 'http://something.com', name: 'custom', icon: icon }
      expect(response).to have_http_status :forbidden
    end

    it 'cannot destroy a link' do
      link = create(:external_link)
      @object.external_links << link
      expect do
        delete :destroy_link, params: { id: @object.id, link_id: link.id }
      end.to change(ExternalLink, :count).by(0)
      expect(response).to have_http_status :forbidden
    end
  end

  context 'not signed in' do
    it 'can get the links' do
      links = create_list(:external_link, 3)
      links.each { |link| @object.external_links << link }

      get :index_link, params: { id: @object.id }

      expect(response).to have_http_status :ok
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq 3
      expect(json_response.pluck('id')).to include(*links.map(&:id))
    end

    it 'cannot add a link' do
      icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')
      expect do
        post :create_link, params: { id: @object.id, url: 'http://something.com', name: 'custom', icon: icon }
      end.to change(ExternalLink, :count).by(0)
      expect(response).to have_http_status :unauthorized
    end

    it 'cannot update a link' do
      icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')
      link = create(:external_link)
      @object.external_links << link
      patch :update_link, params: { id: @object.id, link_id: link.id, url: 'http://something.com', name: 'custom', icon: icon }
      expect(response).to have_http_status :unauthorized
    end

    it 'cannot destroy a link' do
      link = create(:external_link)
      @object.external_links << link
      expect do
        delete :destroy_link, params: { id: @object.id, link_id: link.id }
      end.to change(ExternalLink, :count).by(0)
      expect(response).to have_http_status :unauthorized
    end
  end
end
