# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::SkillsController, type: :controller do
  before do
    @user = create(:confirmed_user)
    sign_in @user
    # @project = create(:project, creator_id: @user.id)
    # @need = create(:need, project_id: @project.id, user_id: @user.id)
    5.times { create(:skill) }
  end

  describe '#index' do
    it 'should return list of skills' do
      skill_count = Skill.all.count
      get :index
      expect(response).to have_http_status :ok
      json_response = JSON.parse(response.body)
      expect(json_response.size).to eq skill_count
    end
  end
end
