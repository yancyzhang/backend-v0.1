# frozen_string_literal: true

require 'rails_helper'
require 'sidekiq/testing'
Sidekiq::Testing.fake!

RSpec.describe RecsysUserWorker, type: :worker do
  # let(:time) { (Time.zone.today + 8.hours).to_datetime }
  # let(:schedule_job) { described_class.perfom_at(time, 'Awesome', true) }

  describe 'testing recsys user recommendation' do

    it 'job goes in correct queue' do
      described_class.perform_async
      assert_equal "default", described_class.queue
    end
  end
end
