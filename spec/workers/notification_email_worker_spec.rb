# frozen_string_literal: true

require 'rails_helper'
# require 'sidekiq/testing'

RSpec.describe NotificationEmailWorker, type: :worker do
  describe 'testing worker' do
    context 'emailing and rendering' do
      before do
        @user = create(:user)
        @user2 = create(:user, first_name: 'Shae', last_name: 'Shoe')
        @notification = @user.notif_new_follower(@user2)
      end

      it 'should sends to right email' do
        mail = NotificationMailer.notify(@notification)
        expect(mail.to).to eq([@user.email])
      end

      it 'should renders the subject' do
        mail = NotificationMailer.notify(@notification)
        expect(mail.subject).to eq('Shae Shoe now follows you!')
      end
    end

    context 'new_post' do
      before do
        @shania = create(:user, email: 'shania@cma.com')
        @project = create(:project, creator: create(:user, first_name: 'Snap', last_name: 'Fire'), title: 'Dota 2')
        @shania.owned_relations.create(resource: @project, follows: true)
        @post = create(:post, feed: @project.feed, user: @project.creator, from: @project)
      end

      it 'should sends to right email' do
        expect do
          @notifications = @project.feed.notif_new_post_in_feed(@post, @project)
        end.to change { described_class.jobs.size }.by(1)

        expect(@notifications.size).to eq(1)

        mail = NotificationMailer.notify(@notifications.first)
        expect(mail.to).to eq([@shania.email])
        expect(mail.subject).to eq('Snap Fire posted on project "Dota 2"')

        html_body = Nokogiri::HTML.parse(mail.body.raw_source)
        link_match = html_body.at_css('p[data-test="link"]')
        expect(link_match.inner_html).to match(%r{<a href="http://localhost:3000/user/#{@project.creator.id}+" style="color:#4B6AF8!important">Snap Fire</a>})
        expect(link_match.inner_html).to match(%r{<a href="http://localhost:3000/post/#{@post.id}+" style="color:#4B6AF8!important">new post</a>})
        expect(link_match.inner_html).to match(%r{<a href="http://localhost:3000/project/#{@project.id}" style="color:#4B6AF8!important">project</a> you are following})
        expect(link_match.inner_text).to match(/Snap Fire posted a new post on a project you are following./)

        check_it_out_match = html_body.at_css('a[data-test="check-it-out"]')
        expect(check_it_out_match.attributes['href'].value).to match(%r{http://localhost:3000/project/#{@project.id}})
        expect(check_it_out_match.inner_text).to match(/Check it out!/)
      end
    end

    # replace with notification target.settings.categories!.send("#{category}!").enabled = true
    xcontext 'user with email notifications disabled' do
      it 'should not send email ' do
        # allow(Invitation).to receive(:deliver)
        # Invitation.deliver
        # expect(Invitation).to have_received(:deliver)
        user = create(:user, email_notifications_enabled: false, status: :active)

        user2 = create(:user)
        notification = user.notif_new_follower(user2)

        mailer = double('mailer')
        allow(mailer).to receive(:deliver)
        allow(NotificationMailer).to receive(:notify).and_return(mailer)

        worker = NotificationEmailWorker.new
        worker.perform(notification.id)
        expect(NotificationMailer).to_not have_received(:notify)
      end
    end

    # replace with notification target.settings.categories!.send("#{category}!").enabled = true
    xcontext 'user with email notifications enabled' do
      it 'should send email' do
        user = create(:user, email_notifications_enabled: false, status: :active)

        user2 = create(:user)
        notification = user.notif_new_follower(user2)

        mailer = double('mailer')
        allow(mailer).to receive(:deliver)
        allow(NotificationMailer).to receive(:notify).and_return(mailer)

        worker = NotificationEmailWorker.new
        worker.perform(notification.id)
        expect(NotificationMailer).to have_received(:notify)
      end
    end
  end
end
