require 'rails_helper'
require 'sidekiq/testing'
Sidekiq::Testing.fake!

RSpec.describe RecsysWorker, type: :worker do

  describe 'testing recsys recommendations' do

    it 'job goes in correct queue' do
      described_class.perform_async
      assert_equal "default", described_class.queue
    end
  end
end
