# frozen_string_literal: true

require 'rails_helper'
# require 'sidekiq/testing'

RSpec.describe InviteUserEmailWorker, type: :worker do
  describe 'testing worker' do
    # it "should respond to #perform" do
    #   owner = create(:user)
    #   object = create(:project, creator_id: owner.id)
    #   joiner = create(:user)
    #   expect{InviteUserEmailWorker.perform_async(owner.id, object.class.to_s, object.id, joiner.id)}.to change{InviteUserEmailWorker.jobs.count}.by(1)
    # end

    # describe "perform" do
    #   before do
    #     factories.clear
    #     Sidekiq::Worker.clear_all
    #   end
    # end

    it 'should sends to right email' do
      owner = create(:user)
      object = create(:project, creator_id: owner.id)
      joiner = create(:user)
      mail = InviteUserMailer.invite_user(owner, object, joiner)
      expect(mail.to).to eq [joiner.email]
    end

    it 'should renders the subject' do
      owner = create(:user)
      object = create(:project, creator_id: owner.id)
      joiner = create(:user)
      mail = InviteUserMailer.invite_user(owner, object, joiner)
      expect(mail.subject).to eq 'You have been invited to join a project'
    end
  end
end
