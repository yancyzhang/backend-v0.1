# frozen_string_literal: true

FactoryBot.define do
  factory :recsys_result do
    sourceable_node_type { 'MyString' }
    sourceable_node_id { '' }
    targetable_node_type { 'MyString' }
    targetable_node_id { '' }
    relation_type { 'MyString' }
    value { 1.5 }
  end
end
