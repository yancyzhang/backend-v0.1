# frozen_string_literal: true

FactoryBot.define do
  factory :community do
    creator

    title { FFaker::Movie.unique.title }
    short_title { FFaker::Internet.unique.user_name }
    logo_url { FFaker::Avatar.image }
    description { FFaker::DizzleIpsum.paragraphs }
    short_description { FFaker::DizzleIpsum.paragraph }

    latitude { rand(-90.000000000...90.000000000) }
    longitude { rand(-180.000000000...180.000000000) }

    status { 0 }

    after :create do |community|
      skills = create_list(:skill, 5)
      skills.map do |skill|
        community.skills << skill # has_many
      end
      ressources = create_list(:ressource, 5)
      ressources.map do |ressource|
        community.ressources << ressource # has_many
      end
      community.creator.add_role :member, community
      community.creator.add_role :admin, community
      community.creator.add_role :owner, community
    end
  end

  factory :public_community, parent: :community do
    is_private { false }
  end

  factory :private_community, parent: :community do
    is_private { true }
  end

  factory :archived_community, parent: :project do
    status { 1 }
  end
end
