# frozen_string_literal: true

FactoryBot.define do
  factory :faq do
    after :create do |faq|
      documents = create_list(:document, 5)
      documents.map do |document|
        faq.documents << document # has_many
      end
      if faq.faqable.nil?
        faq.faqable = create(:program)
        faq.save
      end
    end
  end
end
