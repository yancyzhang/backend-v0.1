# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Dataset, type: :model do
  describe 'associations' do
    it { should have_many(:data) }
    it { should have_many(:sources) }
    it { should have_many(:tags) }
    it { should have_one(:license) }
    it { should belong_to(:author) }
  end
end
