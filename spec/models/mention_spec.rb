# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mention, type: :model do
  describe 'association' do
    it { should belong_to(:post) }
  end

  describe 'validation' do
    let!(:post) { create(:post) }
    let!(:mention) { create(:mention, post: post, obj: post.user) }
    it 'should have valid factory' do
      expect(mention).to be_valid
    end

    it 'should have unique post' do
      mention2 = build(:mention, post: mention.post, obj: post.user)
      expect(mention2).not_to be_valid
    end
  end
end
