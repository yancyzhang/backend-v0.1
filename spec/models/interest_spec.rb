# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Interest, type: :model do
  describe 'associations' do
    it { should have_and_belong_to_many(:users) }
    it { should have_and_belong_to_many(:projects) }
    it { should have_and_belong_to_many(:community) }
    it { should have_and_belong_to_many(:challenge) }
  end
end
