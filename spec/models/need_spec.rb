# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/feedable'

RSpec.describe Need, type: :model do
  it_behaves_like 'a model that can have a feed', :need

  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:project) }
    it { should have_one(:feed) }
  end

  describe 'validation' do
    it 'should have valid factory' do
      expect(build(:need)).to be_valid
    end

    it 'should require title' do
      need = build(:need, title: '')
      expect(need).not_to be_valid
    end

    it 'should require content' do
      need = build(:need, content: '')
      expect(need).not_to be_valid
    end
  end
end
