# frozen_string_literal: true

ActionMailer::Base.register_preview_interceptor(PostmarkRails::PreviewInterceptor) if ActionMailer::Base.postmark_settings[:api_token].present?
