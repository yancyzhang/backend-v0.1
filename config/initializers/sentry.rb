# frozen_string_literal: true

Sentry.init do |config|
  config.environment = ENV['HEROKU_ENV'] if ENV['HEROKU_ENV']
end
