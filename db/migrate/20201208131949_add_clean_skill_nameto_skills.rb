class AddCleanSkillNametoSkills < ActiveRecord::Migration[5.2]
  def change
    add_column :skills, :clean_skill_name, :string
  end
end
