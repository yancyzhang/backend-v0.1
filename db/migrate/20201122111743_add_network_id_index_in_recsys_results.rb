class AddNetworkIdIndexInRecsysResults < ActiveRecord::Migration[5.2]
  def change
    add_index :recsys_results, :network_id, unique: false
  end
end
