class FixChallengeProject < ActiveRecord::Migration[5.2]
  def up
    add_column :challenges_projects, :project_status, :integer
    ChallengesProject.all.each do |challenge_project|
      if challenge_project.status == "accept"
        challenge_project.accepted!
      else
        challenge_project.pending!
      end
    end
    remove_column :challenges_projects, :status
  end

  def down
    add_column :challenges_projects, :status, :string
    ChallengesProject.all.each do |challenge_project|
      if challenge_project.accepted?
        challenge_project.status = "accept"
      elsif challenge_project.pending?
        challenge_project.status = "pending"
      end
    end
    remove_column :challenges_projects, :project_status
  end
end
