# frozen_string_literal: true

class CreateJointTableProjectInterests < ActiveRecord::Migration[5.2]
  def change
    create_table :interests_projects, id: false do |t|
      t.integer :project_id
      t.integer :interest_id
    end
  end
end
