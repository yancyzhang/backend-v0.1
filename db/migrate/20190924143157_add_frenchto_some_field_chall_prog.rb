# frozen_string_literal: true

class AddFrenchtoSomeFieldChallProg < ActiveRecord::Migration[5.2]
  def change
    add_column :challenges, :title_fr, :string
    add_column :challenges, :description_fr, :string
    add_column :challenges, :short_description_fr, :string
    add_column :challenges, :rules_fr, :string
    add_column :challenges, :faq_fr, :string
    add_column :programs, :title_fr, :string
    add_column :programs, :short_title_fr, :string
    add_column :programs, :description_fr, :string
    add_column :programs, :short_description_fr, :string
    add_column :programs, :faq_fr, :string
    add_column :programs, :enablers_fr, :string
  end
end
