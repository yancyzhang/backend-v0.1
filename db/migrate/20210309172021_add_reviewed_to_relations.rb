class AddReviewedToRelations < ActiveRecord::Migration[6.1]
  def change
    add_column :relations, :reviewed, :boolean, default: false
  end
end
