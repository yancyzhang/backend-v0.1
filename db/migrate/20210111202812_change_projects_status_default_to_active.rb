class ChangeProjectsStatusDefaultToActive < ActiveRecord::Migration[6.1]
  def change
    # 0 is "active" status for enum :status
    change_column_default(:projects, :status, from: nil, to: 0)
  end
end
