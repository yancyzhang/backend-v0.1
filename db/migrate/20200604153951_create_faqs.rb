class CreateFaqs < ActiveRecord::Migration[5.2]
  def change
    create_table :faqs do |t|
      t.bigint :faqable_id
      t.string :faqable_type
      t.timestamps
    end
  end
end
