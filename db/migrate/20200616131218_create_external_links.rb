class CreateExternalLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :external_links do |t|
      t.string :linkable_type
      t.bigint :linkable_id
      t.string :url
      t.timestamps
    end
  end
end
