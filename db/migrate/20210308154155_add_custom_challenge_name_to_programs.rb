class AddCustomChallengeNameToPrograms < ActiveRecord::Migration[6.1]
  def change
    add_column :programs, :custom_challenge_name, :string
  end
end
