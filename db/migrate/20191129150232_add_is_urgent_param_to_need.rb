# frozen_string_literal: true

class AddIsUrgentParamToNeed < ActiveRecord::Migration[5.2]
  def change
    add_column :needs, :is_urgent, :boolean, default: false
  end
end
