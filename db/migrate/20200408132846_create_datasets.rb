# frozen_string_literal: true

class CreateDatasets < ActiveRecord::Migration[5.2]
  def change
    create_table :datasets do |t|
      t.string :title
      t.string :short_title
      t.string :description
      t.string :author_id
      t.string :url

      t.string :provider
      t.string :provider_id
      t.string :provider_type
      t.string :provider_url
      t.string :provider_author
      t.string :provider_authoremail
      t.string :provider_version
      t.datetime :provider_created_at
      t.datetime :provider_updated_at

      t.string :type, null: false
      t.bigint  :datasetable_id
      t.string  :datasetable_type
      t.timestamps
    end
  end
end
