# frozen_string_literal: true

class InviteUserMailer < ApplicationMailer
  def invite_user(owner, object, joiner)
    @to = joiner
    @owner = owner
    @object = object
    @from_object = object.class.name.downcase != 'community' ? object.class.name.downcase : 'group'
    @joiner = joiner
    # Postmark metadatas
    metadata['object-type'] = @from_object
    mail(to: "<#{joiner.email}>", subject: 'You have been invited to join a ' + @from_object, tag: 'invite-user')
  end
end
