# frozen_string_literal: true

class PrivateMailer < ApplicationMailer
  def send_private_email(from, to, object, content)
    @to = to
    @from = from
    @object = object
    @content = content
    # Postmark metadatas
    metadata['user-id-from'] = @from.id
    metadata['user-id-to'] = @to.id
    mail(to: "#{@to.first_name} #{@to.last_name} <#{@to.email}>",
         from: "#{@from.first_name} #{@from.last_name} via JOGL <#{ENV['JOGL_NOTIFICATIONS_EMAIL']}>",
         reply_to: "#{@from.first_name} #{@from.last_name} <#{@from.email}>",
         subject: "#{@from.first_name} sent you a message",
         tag: 'private-message')
  end
end
