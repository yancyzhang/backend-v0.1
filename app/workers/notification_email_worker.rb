# frozen_string_literal: true

class NotificationEmailWorker
  include Sidekiq::Worker

  def perform(notification_id)
    return unless (notification = Notification.find_by(id: notification_id))

    NotificationMailer.notify(notification).deliver
  end
end
