# frozen_string_literal: true

class InviteUserEmailWorker
  include Sidekiq::Worker

  def perform(user_id, object_type, object_id, joiner_id)
    @owner = User.find(user_id)
    @joiner = User.find(joiner_id)
    unless @joiner.deleted?
      @object = object_type.constantize.find(object_id)
      InviteUserMailer.invite_user(@owner, @object, @joiner).deliver
    end
  end
end
