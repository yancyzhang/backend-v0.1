# frozen_string_literal: true

require 'platform-api'

class RecsysWorker
  include Sidekiq::Worker

  def deliver_recommendations
    # The RECSYS_ALLOWLIST environment variable can contain a comma separated list
    # of user ids that are allowed to receive recsys emails.  This feature
    # is only needed during the alpha and beta rollouts of the recommendation
    # system and needs to be removed once it is ready for a production rollout.

    users = if ENV['RECSYS_ALLOWLIST'].present?
      user_ids = ENV['RECSYS_ALLOWLIST'].split(',').map(&:strip)
      User.confirmed.where(id: user_ids)
    else
      User.confirmed
    end

    users.find_each do |user|
      RecsysUserWorker.perform_async(user.id)
    end
  end

  def perform
    # Create new recommendations
    # Instanciate to the Heorku API to launch a one off worker from our recsys docker image
    heroku = PlatformAPI.connect_oauth(ENV['HEROKU_API_TOKEN'])
    # Create the body of the requests
    # type is the name of the docker image (that's a bit confusing)
    body={"command": "python -m jogl.recsys -c jogl-recsys-config-v0.yaml",
          "size": ENV['HEROKU_RECSYS_MACHINE_SIZE'],
          "type": "recsys",
          "time_to_live": 1800}

    dyno = heroku.dyno.create(ENV['HEROKU_APP'], body=body)
    has_run = false

    begin
      while true
      	infos = heroku.dyno.info(ENV['HEROKU_APP'], dyno['name'])
      	if infos['state'] == "up"
      		has_run = true
      	end
      end
    rescue Excon::Error::NotFound
    	if has_run
        deliver_recommendations()
      else
        return
      end
    end
  end
end
