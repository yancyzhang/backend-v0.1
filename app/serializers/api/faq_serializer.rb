# frozen_string_literal: true

class Api::FaqSerializer < ActiveModel::Serializer
  has_many :documents
end
