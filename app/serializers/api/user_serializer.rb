# frozen_string_literal: true

class Api::UserSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Utilsserializerhelper

  # use UserSerializerWithPrivateFields if you wish to include private
  # fields in the response, e.g. age, birth_date, gender, etc.
  attributes :active_status,
             :affiliation,
             #  :age,
             :badges,
             :bio,
             #  :birth_date,
             :can_contact,
             :category,
             :city,
             :confirmed_at,
             :country,
             :current_sign_in_at,
             :feed_id,
             :first_name,
             #  :gender,
             :id,
             :interests,
             :last_name,
             :logo_url_sm,
             :logo_url,
             :mail_newsletter,
             :mail_weekly,
             :nickname,
             :ressources,
             :short_bio,
             :skills,
             :stats,
             :status

  attribute :geoloc, unless: :scope?
  attribute :has_clapped, unless: :scope?
  attribute :has_followed, unless: :scope?
  attribute :has_saved, unless: :scope?
  attribute :is_admin, unless: :scope?
  attribute :is_reviewer, unless: :scope?

  def scope?
    defined?(current_user).nil?
  end

  def mutual_count
    if defined?(current_user).nil? || current_user.nil?
      0
    else
      object.follow_mutual_count(current_user)
    end
  end

  def stats
    {
      saves_count: object.saves_count,
      claps_count: object.claps_count,
      follower_count: object.follower_count,
      following_count: object.following_count,
      mutual_count: mutual_count,
      projects_count: object.projects_count,
      needs_count: object.needs_count,
      communities_count: object.communities_count,
      challenges_count: object.challenges_count,
      programs_count: object.programs_count,
      spaces_count: object.spaces_count,
      reviews_count: object.reviews_count
    }
  end
end
