# frozen_string_literal: true

class Api::MembersSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes  :id,
              :first_name,
              :last_name,
              :nickname,
              :status,
              :skills,
              :ressources,
              :interests,
              :can_contact,
              :current_sign_in_at,
              :logo_url,
              :logo_url_sm,
              :short_bio,
              :owner,
              :admin,
              :member,
              :reviewer,
              :pending,
              :has_followed,
              :has_clapped,
              :has_saved,
              :geoloc,
              :stats

  def owner
    object.has_role?(:owner, @instance_options[:parent])
  end

  def admin
    object.has_role?(:admin, @instance_options[:parent])
  end

  def member
    # TODO: run this once so if user joins a project, they
    # TODO: become a member of the program

    # we pull members from a tree of program -> challenges -> projects
    # so if the user is a member of any object in the tree, should be
    # true here.
    return true if object.has_role?(:member, @instance_options[:parent])

    @instance_options[:parent].user_is_member?(object)
  end

  def reviewer
    object.has_role?(:reviewer, @instance_options[:parent])
  end

  def pending
    object.has_role?(:pending, @instance_options[:parent])
  end

  def mutual_count
    if defined?(current_user).nil? || current_user.nil?
      0
    else
      object.follow_mutual_count(current_user)
    end
  end

  def stats
    {
      mutual_count: mutual_count,
      projects_count: object.projects_count
    }
  end
end
