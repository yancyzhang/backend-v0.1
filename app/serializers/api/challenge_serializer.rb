# frozen_string_literal: true

class Api::ChallengeSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :id,
             :title,
             :title_fr,
             :banner_url,
             :banner_url_sm,
             :logo_url,
             :logo_url_sm,
             :short_title,
             :short_description,
             :short_description_fr,
             :rules,
             :rules_fr,
             :faq,
             :faq_fr,
             :status,
             :skills,
             :interests,
             :documents,
             :geoloc,
             :country,
             :city,
             :address,
             :feed_id,
             :users_sm,
             :program,
             :spaces,
             :launch_date,
             :end_date,
             :final_date,
             :claps_count,
             :follower_count,
             :members_count,
             :saves_count,
             :needs_count,
             :projects_count,
             :created_at,
             :updated_at,
             :posts_count

  attribute :is_owner, unless: :scope?
  attribute :is_admin, unless: :scope?
  attribute :is_member, unless: :scope?
  attribute :has_clapped, unless: :scope?
  attribute :has_followed, unless: :scope?
  attribute :has_saved, unless: :scope?
  # Show following attributes only if show_objects is true (set in controller, usually true only for :show api call)
  attribute :description, if: :show_objects?
  attribute :description_fr, if: :show_objects?
  attribute :rules, if: :show_objects?
  attribute :rules_fr, if: :show_objects?
  attribute :faq, if: :show_objects?
  attribute :faq_fr, if: :show_objects?

  def program
    @program = object.program
    if @program.nil?
      {
        id: -1
      }
    else
      {
        id: @program.id,
        title: @program.title,
        title_fr: @program.title_fr,
        short_title: @program.short_title,
        custom_challenge_name: @program.custom_challenge_name
      }
    end
  end

  def spaces
    Affiliation.where(affiliate_type: 'Challenge', parent_type: 'Space', affiliate_id: object.id)
      .map do |affiliation_space|
      Space.where(id: affiliation_space.parent_id).map do |space|
        {
          id: space.id,
          title: space.title,
          short_title: space.short_title,
          short_description: space.short_description,
          banner_url: space.banner_url,
          banner_url_sm: space.banner_url_sm,
          status: space.status,
          custom_challenge_name: space.custom_challenge_name,
          affiliation_id: affiliation_space.id,
          affiliation_status: affiliation_space.status
        }
      end
    end
  end

  def show_objects?
    @instance_options[:show_objects]
  end
end
