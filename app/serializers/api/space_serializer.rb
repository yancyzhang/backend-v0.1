# frozen_string_literal: true

class Api::SpaceSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :available_tabs,
             :banner_url_sm,
             :banner_url,
             :claps_count,
             :code_of_conduct,
             :contact_email,
             :custom_challenge_name,
             :documents,
             :end_date,
             :feed_id,
             :follower_count,
             :home_header,
             :home_info,
             :id,
             :launch_date,
             :logo_url_sm,
             :logo_url,
             :meeting_information,
             :members_count,
             :needs_count,
             :onboarding_steps,
             :posts_count,
             :projects_count,
             :ressources,
             :saves_count,
             :selected_tabs,
             :short_description_fr,
             :short_description,
             :short_title_fr,
             :short_title,
             :show_featured,
             :space_type,
             :status,
             :title_fr,
             :title,
             :users_sm

  attribute :is_owner, unless: :scope?
  attribute :is_admin, unless: :scope?
  attribute :is_member, unless: :scope?
  attribute :has_clapped, unless: :scope?
  attribute :has_followed, unless: :scope?
  attribute :has_saved, unless: :scope?
  # Show following attributes only if show_objects is true (set in controller, usually true only for :show api call)
  attribute :description, if: :show_objects?
  attribute :description_fr, if: :show_objects?
  attribute :enablers, if: :show_objects?
  attribute :enablers_fr, if: :show_objects?

  def show_objects?
    @instance_options[:show_objects]
  end

  def ressources
    object.ressources
  end
end
