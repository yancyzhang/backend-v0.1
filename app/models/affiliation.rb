class Affiliation < ApplicationRecord
  belongs_to :affiliate, polymorphic: true
  belongs_to :parent, polymorphic: true

  enum status: %i[pending accepted]
end
