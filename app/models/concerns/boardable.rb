# frozen_string_literal: true

module Boardable
  extend ActiveSupport::Concern

  included do
    has_many :boards, as: :boardable, dependent: :destroy
  end
end
