# frozen_string_literal: true

module Coordinates
  # def create_avatars
  #   self.logo_url = "https://robohash.org/" + self.short_title + "?set=set4"
  #   self.banner_url = "https://robohash.org/" + self.short_title + "?set=set4"
  # end

  def create_coordinates
    self.latitude = 38.074314 + (rand * 2)
    self.longitude = -43.014136 + (rand * 2)
  end

  # def create_avatar
  # 	# self.logo_url = "https://robohash.org/" + self.email + "?set=set4"
  # 	randomNumber = rand(1..12) # with 8 = max, and 2 = min
  # 	imagesCodes = ["2e46be09", "639bc585", "ee2eb97c", "71d8e3fb", "008eb222", "ec11ec0a", "81c9a54a", "284d4d91", "ce49c345", "7692ba90", "748860b8", "0271ffd0"]
  #   self.logo_url = "/static/media/default-user-"+randomNumber+"."+imagesCodes[randomNumber-2]+".png"
  # end
end
