# frozen_string_literal: true

class Board < ApplicationRecord
  has_many :users_board
  has_many :users, through: :users_board
  belongs_to :boardable, polymorphic: true, optional: true

  validates :title, :description, presence: true
end
