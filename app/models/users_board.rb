# frozen_string_literal: true

class UsersBoard < ApplicationRecord
  belongs_to :user
  belongs_to :board
end
