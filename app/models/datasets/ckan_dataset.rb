# frozen_string_literal: true

require 'open-uri'

class Datasets::CkanDataset < ::Dataset
  before_validation :parse_url

  validate :validate_url

  after_validation :save_metadata
  after_save :set_data
  after_save :set_tags
  after_save :set_sources
  after_save :set_license

  before_update :remove_data
  before_update :remove_tags
  before_update :remove_sources
  before_update :remove_license

  def remove_data
    data.all(&:destroy)
  end

  def remove_tags
    tags.all(&:destroy)
  end

  def remove_sources
    sources.all(&:destroy)
  end

  def remove_license
    license.destroy
  end

  def save_metadata
    unless errors.count > 0
      self.title = @dataset['title']
      self.short_title = @dataset['name']
      self.description = @dataset['description']
      self.provider = %r{(http|https)://([a-zA-Z0-9.]*).*}.match(url)[2]
      self.provider_id = @dataset['id']
      self.provider_type = 'CKAN'
      self.provider_url = @match[1]
      self.provider_author = @dataset['author']
      self.provider_authoremail = @dataset['author_email']
      self.provider_version = @dataset['version']
      self.provider_created_at = @dataset['metadata_created']
      self.provider_updated_at = @dataset['metadata_modified']
    end
  end

  def set_license
    api_url = @match[1] + '/api/action/license_list'
    response = JSON.parse(URI.open(api_url).read)
    response['result'].each do |result|
      next unless result['id'] == @dataset['license_id']

      license = License.new(
        title: result['title'],
        short_title: result['id'],
        url: result['url']
      )
      license.licenseable = self
      self.license = license
    end
  end

  def set_tags
    @dataset['tags'].each do |tag|
      t = Tag.new(name: tag['name'])
      t.tagable = self
      error.add('Error creating tag ?') unless t.save!
      tags << t
    end
  end

  def set_data
    @dataset['resources'].each do |resource|
      data = Datum.new
      data.title = resource['name']
      data.description = resource['description']
      data.url = resource['url']
      data.format = resource['format']
      data.filename = resource['filename']
      data.provider_id = resource['id']
      data.provider_created_at = resource['created']
      data.provider_updated_at = resource['last_modified']
      data.author = author
      data.dataset = self
      errors.add(:data, 'Error creating datum !') unless data.save!
      resource['fields']&.each do |field|
        f = Datafield.new(name: field['name'],
                          title: field['title'],
                          description: field['description'],
                          format: field['format'],
                          unit: field['unit'])
        f.datum = data
        data.datafields << f
      end
      self.data << data
    end
  end

  def set_sources
    @dataset['sources']&.each do |source_id|
      url = @match[1] + '/api/action/organization_show?id=' + source_id
      response = JSON.parse(URI.open(url).read)
      source_url = provider_url + '/dataset?organization=' + response['result']['name']
      source = Source.new(title: response['result']['title'], url: source_url)
      source.sourceable = self
      sources << source
    end
  end

  def dataset_exists
    url = @match[1] + '/api/action/package_show?id=' + @match[3]
    response = JSON.parse(URI.open(url).read)
    if response['success']
      @dataset = response['result']
    else
      errors.add(:url, 'dataset does not exists')
    end
  end

  def api_is_live
    api = @match[1] + '/api'
    response = JSON.parse(URI.open(api).read)
    errors.add(:api, 'is not responding') if response.nil?
    errors.add(:api, 'version is unknown !') if response['version'] > 3
  end

  def validate_url
    errors.add(:url, 'does not contain http or https protocol') if %r{^(https|http)://}.match(url).nil?
    if @match.nil? || (@match.length != 4)
      errors.add(:url, 'is missing or  does not match CKAN dataset format')
    else
      api_is_live
      dataset_exists
    end
  end

  def parse_url
    @match = %r{^((https|http)://[a-zA-Z0-9.]*)/dataset/(\S*)}.match(url)
  end
end
