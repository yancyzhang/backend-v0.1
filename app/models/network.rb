# frozen_string_literal: true

class Network < ApplicationRecord
  has_one_attached :node_list
  has_one_attached :edge_list
end
