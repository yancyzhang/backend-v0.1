# frozen_string_literal: true

class Slackhook < ApplicationRecord
  belongs_to :externalhook
end
