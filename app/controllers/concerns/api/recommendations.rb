# frozen_string_literal: true

module Api::Recommendations
  extend ActiveSupport::Concern

  included do
    before_action :recommender_authenticate_user!, only: [:recommended]
  end

  def recommended
    results = RecsysResult.where(sourceable_node: current_user, targetable_node_type: controller_name.classify)
    render json: results, each_serializer: Api::RecommendationsSerializer
  end

  def similar
    results = RecsysResult.where(sourceable_node: @obj, targetable_node_type: controller_name.classify)
    render json: results, each_serializer: Api::RecommendationsSerializer
  end

  private

  def recommender_authenticate_user!
    authenticate_user!
  end
end
