# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :space, SpaceType, 'Returns a JOGL Space', null: true do
      argument :id, ID, required: true
    end

    field :spaces, [SpaceType], 'Returns the list of JOGL Spaces', null: true

    field :user, UserType, 'Returns a user', null: true do
      argument :id, ID, required: true
    end

    def space(id:)
      Space.find(id)
    end

    def spaces
      Space.all
    end

    def user(id:)
      User.find(id)
    end
  end
end
