# frozen_string_literal: true

module Mutations
  class MarkAllNotificationsAsRead < BaseMutation
    # field :errors, [String], null: false
    field :unread_count, Integer, null: true

    def unread_count
      context[:current_user].notifications.unread.size
    end

    def resolve
      if context[:current_user].notifications.unread.update_all(read: true)
        {
          unread_count: unread_count
        }
      else
        {
          errors: ['something went wrong']
        }
      end
    end
  end
end
